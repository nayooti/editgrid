//
//  EditGrid.swift
//  EditGridTextfield
//
//  Created by Bastian van de Wetering on 18.03.19.
//  Copyright © 2019 Bastian van de Wetering. All rights reserved.
//

import UIKit

class EditTextField: UITextField {
    
    override var text:String? {
        didSet {
            self.selectionBarView.updateBar(selectedIndexes: [1,2])
        }
    }
    
    lazy private var editGrid: EditGrid = {
       let editGrid = EditGrid()
        editGrid.backgroundColor = UIColor.red
        return editGrid
    }()
    
    lazy private var selectionBarView: SelectionBarView = {
        let view = SelectionBarView(textField: self)
        view.backgroundColor = UIColor.white
       return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.font = UIFont.systemFont(ofSize: 34)
        backgroundColor = UIColor.white
        borderStyle = BorderStyle.roundedRect
        leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 30))
        leftViewMode = .always
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() -> UIView {
        let baseView = UIView(frame: .zero)
        baseView.backgroundColor = UIColor.yellow
        baseView.addSubview(self)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.leadingAnchor.constraint(equalTo: baseView.leadingAnchor, constant: 5).isActive = true
        self.topAnchor.constraint(equalTo: baseView.topAnchor, constant: 5).isActive = true
        self.heightAnchor.constraint(equalToConstant: 38).isActive = true
        self.trailingAnchor.constraint(equalTo: baseView.trailingAnchor, constant: -5).isActive = true
        
        baseView.addSubview(selectionBarView)
        selectionBarView.translatesAutoresizingMaskIntoConstraints = false
        selectionBarView.topAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        selectionBarView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        selectionBarView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        selectionBarView.heightAnchor.constraint(equalToConstant: 6).isActive = true
        
        baseView.addSubview(editGrid)
        editGrid.translatesAutoresizingMaskIntoConstraints = false
        editGrid.leadingAnchor.constraint(equalTo: baseView.leadingAnchor, constant: 5).isActive = true
        editGrid.topAnchor.constraint(equalTo: selectionBarView.bottomAnchor, constant: 5).isActive = true
        editGrid.trailingAnchor.constraint(equalTo: baseView.trailingAnchor, constant: -5).isActive = true
        editGrid.bottomAnchor.constraint(equalTo: baseView.bottomAnchor, constant: -5).isActive = true
        return baseView
    }
    
    override func beginFloatingCursor(at point: CGPoint) {
        print(point)
    }
    
    override func updateFloatingCursor(at point: CGPoint) {
        print(point)
    }
    
    override func endFloatingCursor() {
        print("ended")
    }
    
    
    
}

class EditGrid: UIView {
    
    
    
    
}

class SelectionBarView: UIView {
    
    unowned var textField: UITextField
    
    init(textField: UITextField) {
        self.textField = textField;
        super.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateBar(selectedIndexes: [Int]) {
        func drawSelectionTab(minX: CGFloat, maxX: CGFloat, selected: Bool) {
            let selectionBar = UIView()
            selectionBar.backgroundColor = selected ? UIColor.lightGray : UIColor.white
            selectionBar.translatesAutoresizingMaskIntoConstraints = false
            selectionBar.layer.cornerRadius = 6
            selectionBar.clipsToBounds = true
            selectionBar.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            selectionBar.layer.borderColor = UIColor.lightGray.cgColor
            selectionBar.layer.borderWidth = 0.3
            addSubview(selectionBar)
            selectionBar.leadingAnchor.constraint(equalTo: leadingAnchor, constant: minX).isActive = true
            selectionBar.trailingAnchor.constraint(equalTo: leadingAnchor, constant: maxX).isActive = true
            selectionBar.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
            selectionBar.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        }
    
        // remove legacy selectionBars
        self.subviews.forEach({$0.removeFromSuperview()})
        
        guard let text = textField.text else {
            return
        }
        
        var spaceIndexes = text.blankSpaceIndexes()
        spaceIndexes.insert(0, at: 0)
        let beginPosition:UITextPosition = textField.beginningOfDocument
        
        
        for (i, spaceIndex) in spaceIndexes.enumerated() {
            var startPosition:UITextPosition?
            var endPosition:UITextPosition?
            
            var nextIndex:Int =  spaceIndexes.count > i + 1 ? spaceIndexes[i + 1] : text.count - 1
            if i == 0 {
                startPosition = textField.position(from: beginPosition, offset: spaceIndex)
            } else{
                startPosition = textField.position(from: beginPosition, offset: spaceIndex + 1)
            }
            endPosition = textField.position(from: beginPosition, offset: nextIndex)
            
            guard let startPos = startPosition, let endPos = endPosition else {
                break
            }
            let minX = textField.caretRect(for: startPos).minX + 5
            let maxX = textField.caretRect(for: endPos).maxX + 5
            drawSelectionTab(minX: minX, maxX: maxX, selected: selectedIndexes.contains(i))
        }
    }
}



