//
//  ViewController.swift
//  EditGridTextfield
//
//  Created by Bastian van de Wetering on 18.03.19.
//  Copyright © 2019 Bastian van de Wetering. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    lazy var editTextField: EditTextField = {
        let textField = EditTextField(frame: .zero)

        textField.text = "0123 45 6789 1012 3456"
        return textField
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view.backgroundColor = UIColor.white
        setupTextfield()
    }
    
    func setupTextfield(){
        
        let safeArea = view.safeAreaLayoutGuide
        let editGridView = editTextField.setup()
        
        
        view.addSubview(editGridView)
        editGridView.translatesAutoresizingMaskIntoConstraints = false
        editGridView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 10).isActive = true
        editGridView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 10).isActive = true
        editGridView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -10).isActive = true
        editGridView.heightAnchor.constraint(equalToConstant: 200).isActive = true
    }


}

