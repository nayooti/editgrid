//
//  Extensions.swift
//  EditGridTextfield
//
//  Created by Bastian van de Wetering on 19.03.19.
//  Copyright © 2019 Bastian van de Wetering. All rights reserved.
//

import UIKit

extension String {
    func blankSpaceIndexes() -> [Int] {
        var spaceIndexes:[Int] = []
        for (index,char) in self.enumerated() {
            if char == " " {
                spaceIndexes.append(index)
            }
        }
        return spaceIndexes
    }
}

